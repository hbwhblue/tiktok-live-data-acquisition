
- [基础-websocket逆向](https://blog.csdn.net/tdl320721/article/details/133515344)
- [基础-http拦截](https://blog.csdn.net/tdl320721/article/details/133522657)
- [基础-websocket拦截](https://blog.csdn.net/tdl320721/article/details/133560565)
- [基础-base64编码与解码](https://blog.csdn.net/tdl320721/article/details/133634544)
- [基础-python实现protobuf序列化与反序列化](https://blog.csdn.net/tdl320721/article/details/133827636)
- [基础-javascript实现protobuf序列化与反序列化](https://blog.csdn.net/tdl320721/article/details/133866583)
- [基础-protobufjs实现protobuf序列化与反序列化](https://blog.csdn.net/tdl320721/article/details/134244821)
- [视频号直播弹幕采集](https://blog.csdn.net/tdl320721/article/details/133635011)
- [tiktok protobuf序列化与反序列化](https://blog.csdn.net/tdl320721/article/details/133829256)
- [实战一：Http轮询](https://blog.csdn.net/tdl320721/article/details/133745779)
- [更新中](https://www.qiulianmao.com/)
# 一、websocket逆向实际应用
主要介绍websocket逆向的**实际用途**、**直播间常见通信技术**、**websocket逆向技术**以及**学习路线**
## 1. 插件开发
1. 语言播报
2. 刷礼物加播
3. 自动点歌
4. 弹幕互动插件
## 2. 弹幕互动游戏
1. 和游戏交互
2. 和硬件交互
3. 整盅玩法
## 3. (半)无人直播
1. 数字人
2. ...
---
# 二、直播间常见通信技术
`下面章节会详细讲解`
## 1. http轮询
以前直播间大多采用http轮询，现在越来越少，但还是有用的，比如视频号助手。或者把http轮询作为一个备用方案，当websocket发生阻塞的时候，就会启用http轮询，保持直播间的通信，例如国内最火的短视频平台。
## 2. websocket通信
现在直播间基本采用websocket进行通信，如果把http轮询比作发短信，那websocket可以认为是打电话。
# 三. 直播间常见消息解析技术
`客户端接收的消息，会有不同格式，这也决定了逆向的难度。`
1. 明文的json字符串或者二进制字符串，这种是最简单的，只需要进行简单的反序列化或者解码即可使用。
2. protobuf二进制，这种需要还原proto结构，再进行反序列化。
3. 明文+乱码配合使用，这种需要按部就班进行分析。
---
# 四、 逆向直播间常用技术
`下面章节会详细讲解`
## 1. RPC远程服务调用
不需要掌握数字签名认证，只需要逆向找到**消息解析的地方**即可。
## 2. 通信拦截
只需要**掌握消息解析的原理**即可实现，不需要了解数字签名以及校验
## 3. API调用
涉及的验证比较多，难度最高，但是并发高、部署方便。

---
# 五、学习路线
`正在搭建一个实战平台，实战平台采用大厂常用弹幕通信技术，也会出对应的教程`
## 实战1：http轮询
视频号助手采用的就是http轮询，每隔几秒向服务器发起http请求，然后将获取到的内容渲染到浏览器上。逆向技术推荐：http拦截。因为会涉及到扫码登录、数字签名...操作起来比较麻烦，也只能看自己的直播间，所以推荐http拦截。
## 实战2：websocket
这个websocket实战不会涉及到签名算法等，消息也是没有加密的，带大家了解websocket逆向的完整流程。
## 实战3：websocket+protobuf
这个实战训练结合了快手平台的弹幕技术，涉及到protobuf序列化与反序列化。
## 实战4：websocket+protobuf+gzip
这个实战训练结合了海外tiktok的弹幕技术，涉及到websocket地址的组成、protobuf序列化与反序列化...
## 实战5：websocket+protobuf+gizp+数字签名
这个实战训练结合了国内最大的短视频平台的弹幕技术，涉及到数字签名、websocket地址的组成、protobuf序列化与反序列化....
## 实战6：websocket+unicode编码
这个实战训练结合了国外twitch的弹幕传输技术，比较简单，但是会涉及到unicode编码，这个还是很少见的
## 实战7：websocket+其他编码
这个不会涉及到什么消息结构，需要按部就班分析。
