"""
@FileName：反序列化.py
@Description：个人开发者
@Author：秋恋猫
@Time：2023/10/16 16:53
@Website：www.qiulianmao.com
@Copyright：©2022-2023 秋恋猫
"""

import person_pb2


def decode_message(data):
    print(data)
    person = person_pb2.Person()

    person.ParseFromString(data)
    print(person)



if __name__ == '__main__':
    data = b'\n\x02JF\x10\x15\x18\x01"\x10\n\x02qq\x12\n123@qq.com"\x13\n\x02wy\x12\r12345@123.com*\r\n\x06\xe5\xae\x9d\xe9\xa9\xac\x10\x01\x18\xe6\x0f*\r\n\x06\xe5\xa5\x94\xe9\xa9\xb0\x10\x02\x18\xe7\x0f0\x01:\x0bhello world'
    decode_message(data)
