"""
@FileName：序列化.py
@Description：个人开发者
@Author：秋恋猫
@Time：2023/10/16 16:47
@Website：www.qiulianmao.com
@Copyright：©2022-2023 秋恋猫
"""
import person_pb2


def encode_message():
    person1 = person_pb2.Person()
    person1.name = "JF"
    person1.age = 21
    person1.gender = person_pb2.Gender.MALE
    person1.email['qq'] = '123@qq.com'
    person1.email['wy'] = '12345@123.com'

    cars1 = person1.cars.add()
    cars1.maker = "宝马"
    cars1.id = 1
    cars1.year = 2022

    cars1 = person1.cars.add()
    cars1.maker = "奔驰"
    cars1.id = 2
    cars1.year = 2023

    person1.is_student = True
    person1.bytes = b'hello world'
    print(person1)
    data = person1.SerializeToString()
    print(data)




if __name__ == '__main__':
    encode_message()
