const protobuf = require("protobufjs");
const fs = require("fs");

// 加载 .proto 文件
const protoPath = "example.proto";
const root = protobuf.loadSync(protoPath);

// 将整个 .proto 文件转换为 JSON
const jsonDescriptor = root.toJSON();

// 将 JSON 描述写入文件
const outputFileName = "example.json";
console.log(JSON.stringify(jsonDescriptor))
fs.writeFileSync(outputFileName, JSON.stringify(jsonDescriptor, null, 2));

console.log("Proto 文件已成功转换为 JSON 文件.");